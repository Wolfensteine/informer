package com.informer;

import com.informer.authentication.AuthenticationService;
import com.informer.authentication.SimpleLogoutHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
public class MainConfiguration {
    @Bean
    public LogoutSuccessHandler createLogoutHandler(AuthenticationService service) {
        return new SimpleLogoutHandler(service);
    }
}
