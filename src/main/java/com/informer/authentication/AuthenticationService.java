package com.informer.authentication;

import com.informer.User;
import com.informer.token.Token;
import com.informer.token.TokenDAO;
import com.informer.token.TokenFactory;
import com.informer.token.WrongTokenException;
import com.informer.user.UserDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Log4j2
public class AuthenticationService {
    private final UserDAO userDAO;
    private final TokenFactory tokenfactory;
    private final TokenDAO tokenDAO;

    public LoginResult login(LoginInformations loginInformations) {
        Token token = new Token();
        try {
            User user = userDAO.findUser(loginInformations);
            token = tokenfactory.initToken(user);
        } catch (NoResultException ex) {
            log.info("Nie znaleziono uzytkownika: " + loginInformations);
        }
        return new LoginResult(new AuthenticationToken(token.getUserToken()));
    }

    @Transactional
    public void logout(String userToken) throws EmptyTokenException, WrongTokenException {
        Optional.ofNullable(userToken)
                .orElseThrow(EmptyTokenException::new);

        Token token = tokenDAO.findTokenByUserToken(userToken)
                .orElseThrow(() -> new WrongTokenException(new AuthenticationToken(userToken)));
        tokenDAO.removeToken(token);
    }

    private Optional<Token> findToken(User user, String userToken) {
        Set<Token> tokens = user.getTokens();
        return tokens.stream().filter(token -> token.getUserToken().equals(userToken)).findFirst();
    }
}
