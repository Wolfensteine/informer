package com.informer.authentication;

public class EmptyTokenException extends Exception {
    EmptyTokenException() {
        super("Retrieved empty token.");
    }
}
