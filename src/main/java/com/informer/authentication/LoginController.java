package com.informer.authentication;

import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpSession;

public interface LoginController {
    ResponseEntity<?> login(LoginInformations loginInformations, HttpSession session);
}
