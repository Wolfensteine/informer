package com.informer.authentication;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class LoginInformations {
    private String login;
    private String password;
}
