package com.informer.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LoginResult {
    private AuthenticationToken token;

    public boolean hasCorrectToken() {
        return token.getUserToken() != null;
    }
}
