package com.informer.authentication;

import com.informer.token.WrongTokenException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
@RequiredArgsConstructor
public class SimpleLogoutHandler implements LogoutSuccessHandler {
    private final AuthenticationService authenticationService;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            authenticationService.logout(request.getHeader("userToken"));
        } catch (EmptyTokenException | WrongTokenException e) {
            log.debug("", e);
        }
    }
}
