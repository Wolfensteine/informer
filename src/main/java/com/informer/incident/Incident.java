package com.informer.incident;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
public class Incident {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "user_id")
    private int userId;
    @Getter
    @Setter
    private IncidentStatus status;
    @Getter
    @Setter
    private String description;

    Incident(int userId, String description) {
        this.userId = userId;
        this.description = description;
        status = IncidentStatus.NEW;
    }
}
