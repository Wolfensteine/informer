package com.informer.incident;

import com.informer.authentication.AuthenticationToken;
import com.informer.token.WrongTokenException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class IncidentController {

    private final IncidentService service;

    @PutMapping("incident/register")
    public IncidentResult registerNewIncident(
            @RequestHeader String userToken,
            @RequestBody NewIncidentInformations newIncidentInformations) throws WrongTokenException {
        return service.register(newIncidentInformations, new AuthenticationToken(userToken));
    }

    @GetMapping("incidents")
    public List<IncidentResult> getIncidents(@RequestHeader String userToken) {
        return service.getIncidents(userToken);
    }

    @GetMapping("/resource")
    public Map<String, Object> home() {
        Map<String, Object> model = new HashMap<>();
        model.put("id", UUID.randomUUID().toString());
        model.put("content", "Hello World");
        return model;
    }
}
