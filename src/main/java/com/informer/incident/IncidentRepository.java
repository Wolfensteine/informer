package com.informer.incident;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncidentRepository {
    IncidentResult save(Incident incident);

    List<IncidentResult> getIncidentsByUserId(int userId);
}
