package com.informer.incident;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class IncidentResult {
    private final IncidentStatus status;
    private final String description;
}
