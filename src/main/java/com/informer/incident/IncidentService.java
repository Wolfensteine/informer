package com.informer.incident;

import com.informer.authentication.AuthenticationToken;
import com.informer.token.Token;
import com.informer.token.TokenDAO;
import com.informer.token.WrongTokenException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class IncidentService {
    private final IncidentRepository incidentRepository;
    private final TokenDAO tokenDAO;

    public IncidentResult register(NewIncidentInformations newIncidentInformations, AuthenticationToken authenticationToken) throws WrongTokenException {
        Optional<Token> foundToken = tokenDAO.findTokenByUserToken(authenticationToken.getUserToken());
        Token token = foundToken.orElseThrow(() ->
                new WrongTokenException(authenticationToken));
        return incidentRepository.save(new Incident(token.getUser().getId(), newIncidentInformations.getDescription()));
    }

    public List<IncidentResult> getIncidents(String userToken) {
        Optional<Token> foundToken = tokenDAO.findTokenByUserToken(userToken);
        return foundToken.map(token ->
                        incidentRepository.getIncidentsByUserId(token.getUser().getId()))
                .orElse(new ArrayList<>());
    }
}
