package com.informer.incident;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class NewIncidentInformations {
    private final String description;

    public NewIncidentInformations(@JsonProperty("description") String description) {
        this.description = description;
    }
}
