package com.informer.incident;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Repository
public class SqlIncidentRepository implements IncidentRepository {
    private final EntityManager entityManager;

    @Override
    @Transactional
    public IncidentResult save(Incident incident) {
        Incident mergedToken = entityManager.merge(incident);
        return new IncidentResult(mergedToken.getStatus(), mergedToken.getDescription());
    }

    @Override
    @Transactional
    public List<IncidentResult> getIncidentsByUserId(int userId) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<Incident> query = currentSession.createQuery("select i from Incident i where i.userId=:userId", Incident.class);
        query.setParameter("userId", userId);
        List<Incident> incidents = query.getResultList();
        return incidents.stream().map(incident ->
                        new IncidentResult(incident.getStatus(), incident.getDescription()))
                .collect(Collectors.toList());
    }
}
