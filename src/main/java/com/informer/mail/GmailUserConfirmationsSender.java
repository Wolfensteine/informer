package com.informer.mail;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.Properties;

@Component
@RequiredArgsConstructor
public class GmailUserConfirmationsSender implements UserConfirmationsSender {
    private final JavaMailSender mailSender;
    private final MessageCreator messageCreator;

    @Value("${spring.mail.username}")
    private String adressFrom;

    public void sendAccountActivationMail(String login, String email, String token) throws MessagingException {
        MimeMessageHelper messageHelper = new MimeMessageHelper(mailSender.createMimeMessage(), false);
        Properties messageProperties = new Properties();
        messageProperties.put("adressFrom", adressFrom);
        messageProperties.put("login", login);
        messageProperties.put("email", email);
        messageProperties.put("token", token);
        messageHelper.setText(messageCreator.createAccountActivationMessage(messageHelper, messageProperties));
        mailSender.send(messageHelper.getMimeMessage());
    }
}
