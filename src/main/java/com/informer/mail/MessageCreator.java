package com.informer.mail;

import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import java.util.Properties;

public interface MessageCreator {
    String createAccountActivationMessage(MimeMessageHelper messageHelper, Properties properties) throws MessagingException;
}
