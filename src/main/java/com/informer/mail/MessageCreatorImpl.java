package com.informer.mail;

import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.Properties;

@Component
public class MessageCreatorImpl implements MessageCreator {
    @Override
    public String createAccountActivationMessage(MimeMessageHelper messageHelper, Properties properties) throws MessagingException {
        messageHelper.setFrom(properties.getProperty("adressFrom"));
        messageHelper.setTo(properties.getProperty("email"));
        messageHelper.setSubject("Account activation");
        return properties.getProperty("token", "habababa");
    }
}
