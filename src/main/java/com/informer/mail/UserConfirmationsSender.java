package com.informer.mail;

import javax.mail.MessagingException;

public interface UserConfirmationsSender {
    void sendAccountActivationMail(String login, String email, String token) throws MessagingException;
}
