package com.informer.token;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class HibernateTokenDAO implements TokenDAO {
    private final EntityManager entityManager;

    @Override
    @Transactional
    public Optional<Token> findTokenByUserId(int userId) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<Token> query = currentSession.createQuery("select t from Token t where t.user.id=:userId", Token.class);
        query.setParameter("userId", userId);
        List<Token> tokens = query.getResultList();
        return tokens.stream().findFirst();
    }

    @Override
    @Transactional
    public Optional<Token> findTokenByUserToken(String userToken) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<Token> query = currentSession.createQuery("select t from Token t where t.userToken=:userToken", Token.class);
        query.setParameter("userToken", userToken);
        List<Token> tokens = query.getResultList();
        return tokens.stream().findFirst();
    }

    @Override
    public void removeToken(Token token) {
//        entityManager.remove(entityManager.merge(token));
        entityManager.remove(token);
    }

    @Override
    @Transactional
    public void save(Token token) {
        entityManager.merge(token);
    }
}
