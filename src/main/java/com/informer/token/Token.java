package com.informer.token;

import com.informer.User;
import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String userToken;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    Token(String userToken, User user) {
        this.userToken = userToken;
        this.user = user;
    }
}
