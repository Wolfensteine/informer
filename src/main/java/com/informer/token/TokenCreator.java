package com.informer.token;

import com.informer.User;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TokenCreator {
    public Token createToken(User user) {
        String userToken = UUID.randomUUID().toString();
        return new Token(userToken, user);
    }
}
