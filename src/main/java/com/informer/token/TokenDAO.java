package com.informer.token;

import java.util.Optional;

public interface TokenDAO {
    void save(Token token);

    Optional<Token> findTokenByUserId(int id);

    Optional<Token> findTokenByUserToken(String userToken);

    void removeToken(Token token);
}
