package com.informer.token;

import com.informer.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TokenFactory {
    private final TokenDAO tokenDAO;
    private final TokenCreator tokenCreator;

    public Token initToken(User user) {
        Token token = tokenCreator.createToken(user);
        tokenDAO.save(token);
        return token;
    }
}
