package com.informer.token;

import com.informer.authentication.AuthenticationToken;

public class WrongTokenException extends Exception {
    public WrongTokenException(Token token) {
        super("Token not found in database: " + token);
    }

    public WrongTokenException(AuthenticationToken token) {
        super("Token not found in database: " + token);
    }
}
