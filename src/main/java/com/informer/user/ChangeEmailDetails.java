package com.informer.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ChangeEmailDetails {
    private final String newEmail;

    public ChangeEmailDetails(@JsonProperty("newEmail") String newEmail) {
        this.newEmail = newEmail;
    }
}
