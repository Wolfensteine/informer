package com.informer.user;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@EqualsAndHashCode
@RequiredArgsConstructor
public class ChangePasswordDetails {
    private final String currentPassword;
    private final String newPassword;
}
