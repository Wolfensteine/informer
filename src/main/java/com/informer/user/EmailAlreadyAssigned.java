package com.informer.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailAlreadyAssigned extends Exception {
    EmailAlreadyAssigned(String email) {
        super("Email: " + email + " already assigned to different user.");
    }
}
