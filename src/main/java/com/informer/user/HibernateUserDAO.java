package com.informer.user;

import com.informer.User;
import com.informer.authentication.LoginInformations;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Repository
public class HibernateUserDAO implements UserDAO {
    private final EntityManager entityManager;

    @Override
    @Transactional
    public User findUser(LoginInformations loginInformations) throws NoResultException {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<User> query = currentSession.createQuery(
                "select u from User u where u.login=:login and u.password=:password", User.class);
        query.setParameter("login", loginInformations.getLogin());
        query.setParameter("password", loginInformations.getPassword());
        return query.getSingleResult();
    }

    @Override
    @Transactional
    public User registerUser(User user) {
        return entityManager.merge(user);
    }

    @Override
    @Transactional
    public boolean emailRegistered(String email) {
        List<User> users = findUsersByEmail(email);
        return !users.isEmpty();
    }

    @Override
    public Optional<User> findUserByUserToken(String userToken) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<User> query = currentSession.createQuery(
                "select u from User u join u.tokens tokens where tokens.userToken=:userToken", User.class);
        query.setParameter("userToken", userToken);
        return Optional.ofNullable(query.getSingleResult());
    }

    @Override
    @Transactional
    public void changeEmail(User user) {
        entityManager.merge(user);
    }

    @Override
    @Transactional
    public void changePassword(User user) {
        entityManager.merge(user);
    }

    private List<User> findUsersByEmail(String email) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<User> query = currentSession.createQuery(
                "select u from User u where u.email=:email", User.class);
        query.setParameter("email", email);
        return query.getResultList();
    }

    @Override
    public List<User> findUsersByLoginOrEmail(String login, String email) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query<User> query = currentSession.createQuery(
                "select u from User u where u.login=:login or u.email=:email", User.class);
        query.setParameter("login", login);
        query.setParameter("email", email);
        return query.getResultList();
    }

    @Override
    public void save(User user) {
        entityManager.merge(user);
    }
}
