package com.informer.user;

public class LoginAlreadyAssigned extends Exception {
    public LoginAlreadyAssigned(String login) {
        super("Login already assigned: " + login);
    }
}
