package com.informer.user;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;

@RestController
@RequiredArgsConstructor
public class RestUserController {
    private final UserService userService;

    @PostMapping("/user/changeEmail")
    public void changeEmail(@RequestHeader String userToken, @RequestBody ChangeEmailDetails changeEmailDetails)
            throws Throwable {
        userService.changeEmail(userToken, changeEmailDetails);
    }

    @PostMapping("/user/changePassword")
    public void changePassword(@RequestHeader String userToken, @RequestBody ChangePasswordDetails changePasswordDetails)
            throws Exception {
        userService.changePassword(userToken, changePasswordDetails);
    }

    @PostMapping("/user/register")
    public ResponseEntity<String> registerUser(@RequestBody UserRegistrationDetails userRegistrationDetails) {
        try {
            userService.registerUser(userRegistrationDetails);
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (LoginAlreadyAssigned loginAlreadyAssigned) {
            return new ResponseEntity<>("Login exists", HttpStatus.BAD_REQUEST);
        } catch (EmailAlreadyAssigned emailAlreadyAssigned) {
            return new ResponseEntity<>("Email exists", HttpStatus.BAD_REQUEST);
        } catch (MessagingException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
