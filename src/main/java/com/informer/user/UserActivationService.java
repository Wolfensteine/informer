package com.informer.user;

import com.informer.User;
import com.informer.mail.UserConfirmationsSender;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
@RequiredArgsConstructor
public class UserActivationService {
    private final UserConfirmationsSender mailSender;
    private final UserActivationsDAO dao;

    void sendUserActivation(User user) throws MessagingException {
        UserActivation userActivation = dao.addUserActivation(user);
        new DeleteExpiredUserActivationAction().scheduleDeletionExpiredActivation(user);
        mailSender.sendAccountActivationMail(user.getLogin(), user.getEmail(), userActivation.getToken());
    }
}
