package com.informer.user;

import com.informer.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class UserActivationsDAO {
    private final EntityManager entityManager;

    @Transactional
    public UserActivation addUserActivation(User user) {
        UserActivation userActivation = new UserActivation();
        userActivation.setUser(user);
        userActivation.setToken(UUID.randomUUID().toString());
        userActivation.setDate(LocalDateTime.now());
        return entityManager.merge(userActivation);
    }
}
