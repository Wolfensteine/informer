package com.informer.user;

import com.informer.User;
import com.informer.authentication.LoginInformations;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserDAO {
    User findUser(LoginInformations loginInformations) throws NoResultException;

    User registerUser(User user);

    boolean emailRegistered(String email);

    Optional<User> findUserByUserToken(String userToken);

    void changeEmail(User user);

    void changePassword(User user);

    List<User> findUsersByLoginOrEmail(String login, String email);

    void save(User user);
}
