package com.informer.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserNotFound extends Exception {
    public UserNotFound(String userToken) {
        super("User with user token not found: " + userToken);
    }
}
