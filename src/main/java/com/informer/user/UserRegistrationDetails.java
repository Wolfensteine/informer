package com.informer.user;

import lombok.Data;

@Data
public class UserRegistrationDetails {
    private final String login;
    private final String email;
    private final String password;
}
