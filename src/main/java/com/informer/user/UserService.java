package com.informer.user;

import com.informer.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserDAO userDAO;
    private final UserActivationService activationService;

    public void changeEmail(String userToken, ChangeEmailDetails changeEmailDetails) throws UserNotFound,
            WrongEmailException, EmailAlreadyAssigned {
        //TODO walidacja maila, wyslanie powiadomienia na maila
        Optional<User> foundUser = userDAO.findUserByUserToken(userToken);
        User user = foundUser.orElseThrow(() -> new UserNotFound(userToken));
        String newEmail = changeEmailDetails.getNewEmail();
        if (user.getEmail().equals(newEmail)) {
            throw new WrongEmailException(newEmail);
        }
        if (userDAO.emailRegistered(newEmail)) {
            throw new EmailAlreadyAssigned(newEmail);
        }
        user.setEmail(newEmail);
        userDAO.changeEmail(user);
    }

    public void changePassword(String userToken, ChangePasswordDetails changePasswordDetails)
            throws UserNotFound, WrongPasswordException {
        //TODO walidacja jakosci hasla
        Optional<User> foundUser = userDAO.findUserByUserToken(userToken);
        User user = foundUser.orElseThrow(() -> new UserNotFound(userToken));
        if (!user.getPassword().equals(changePasswordDetails.getCurrentPassword())) {
            throw new WrongPasswordException();
        }
        if (user.getPassword().equals(changePasswordDetails.getNewPassword())) {
            throw new WrongPasswordException();
        }
        user.setPassword(changePasswordDetails.getNewPassword());
        userDAO.changePassword(user);
    }

    public void registerUser(UserRegistrationDetails userRegistrationDetails) throws LoginAlreadyAssigned,
            EmailAlreadyAssigned, MessagingException {
        List<User> foundUsers = userDAO.findUsersByLoginOrEmail(userRegistrationDetails.getLogin(),
                userRegistrationDetails.getEmail());
        long usersWithSameLoginCount = foundUsers.stream().filter(user ->
                user.getLogin().equals(userRegistrationDetails.getLogin())).count();
        if (usersWithSameLoginCount > 0) {
            throw new LoginAlreadyAssigned(userRegistrationDetails.getLogin());
        }
        long usersWithSameEmailCount = foundUsers.stream().filter(user ->
                user.getEmail().equals(userRegistrationDetails.getEmail())).count();
        if (usersWithSameEmailCount > 0) {
            throw new EmailAlreadyAssigned(userRegistrationDetails.getEmail());
        }
        User user = new User();
        user.setEmail(userRegistrationDetails.getEmail());
        user.setLogin(userRegistrationDetails.getLogin());
        user.setPassword(userRegistrationDetails.getPassword());
        userDAO.registerUser(user);
        activationService.sendUserActivation(user);
    }
}
