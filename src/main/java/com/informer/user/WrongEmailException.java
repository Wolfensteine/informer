package com.informer.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WrongEmailException extends Throwable {
    public WrongEmailException(String email) {
        super("Provided email is equal with current user email: " + email);
    }
}
