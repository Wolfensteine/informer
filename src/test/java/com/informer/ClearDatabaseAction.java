package com.informer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ClearDatabaseAction {
    private final DataSource dataSource;

    public ClearDatabaseAction(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void run() throws SQLException {
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        statement.execute("TRUNCATE SCHEMA public AND COMMIT;");
    }
}
