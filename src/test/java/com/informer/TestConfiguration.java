package com.informer;

import com.informer.mail.FakeMailSender;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;

@Configuration
public class TestConfiguration {
    @Bean
    public JavaMailSender createMailSender() {
        return new FakeMailSender();
    }

//    @Bean
//    public UserActivationService createActivationService() {
//        return new UserActivationService((login, email, token) -> {
//
//        }, mock(UserActivationsDAO.class));
//    }
}
