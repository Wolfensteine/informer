package com.informer.authentication;

import com.informer.User;
import com.informer.token.Token;
import com.informer.token.TokenDAO;
import com.informer.token.TokenFactory;
import com.informer.token.WrongTokenException;
import com.informer.user.UserDAO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.persistence.NoResultException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AuthenticationServiceTest {

    private static UserDAO userDAO;
    private static TokenFactory tokenFactory;
    private static AuthenticationService service;
    private static TokenDAO tokenDAO;

    @BeforeAll
    public static void setUp() {
        userDAO = mock(UserDAO.class);
        tokenFactory = mock(TokenFactory.class);
        tokenDAO = mock(TokenDAO.class);
        service = new AuthenticationService(userDAO, tokenFactory, tokenDAO);
    }

    @Test
    public void shouldLoginUser() {
        //given
        LoginInformations loginInformations = new LoginInformations("name", "namePassword");
        Token token = new Token();
        token.setUserToken("token");
        AuthenticationToken authenticationToken = new AuthenticationToken(token.getUserToken());
        when(tokenFactory.initToken(any())).thenReturn(token);
        User user = new User();
        user.setLogin("name");
        when(userDAO.findUser(loginInformations)).thenReturn(user);
        //when
        LoginResult result = service.login(loginInformations);
        //then
        assertThat(result.getToken()).isEqualTo(authenticationToken);
    }

    @Test
    public void shouldNotLoginUser() {
        //given
        LoginInformations loginInformations = new LoginInformations("name", "namePassword");
        Token token = new Token();
        token.setUserToken("token");
        when(tokenFactory.initToken(any())).thenReturn(token);
        when(userDAO.findUser(loginInformations)).thenThrow(new NoResultException());
        //when
        LoginResult result = service.login(loginInformations);
        //then
        assertThat(result.getToken().getUserToken()).isNull();
    }

    @Test
    public void shouldLogoutUser() throws WrongTokenException, EmptyTokenException {
        //given
        String userToken = "token";
        Token token = new Token();
        token.setUserToken(userToken);
        when(tokenDAO.findTokenByUserToken(userToken)).thenReturn(Optional.of(token));
        //when
        service.logout(userToken);
        //then
        verify(tokenDAO).removeToken(token);
    }

    @Test
    public void shouldThrowExceptionWhenNullTokenProvided() {
        //given
        //when
        //then
        assertThatThrownBy(() ->
                service.logout(null))
                .isInstanceOf(EmptyTokenException.class);
    }

    @Test
    public void shouldThrowExceptionWhenWrongTokenProvided() {
        //given
        //when
        //then
        assertThatThrownBy(() ->
                service.logout("userTokenNotInDatabase"))
                .isInstanceOf(WrongTokenException.class);
    }
}