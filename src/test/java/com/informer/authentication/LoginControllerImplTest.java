package com.informer.authentication;

import com.informer.JsonConverter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LoginControllerImpl.class)
class LoginControllerImplTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthenticationService authenticationService;

    @Test
    public void shouldLoginUser() throws Exception {
        //given
        String name = "name1";
        String password = "name1Password";
        AuthenticationToken token = new AuthenticationToken("token");
        LoginResult loginResult = new LoginResult(token);
        when(authenticationService.login(any())).thenReturn(loginResult);
        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/login")
                        .with(csrf())
                        .content("{\"login\" : \"" + name + "\",\n" +
                                "\"password\" : \"" + password + "\"\n}")
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().json(JsonConverter.convertObjectToJson(loginResult)))
                .andDo(print());
    }

    @Test
    public void shouldNotLoginUser() throws Exception {
        //given
        String name = "wrongName";
        String password = "wrongNamePassword";
        LoginResult loginResult = new LoginResult(new AuthenticationToken(null));
        when(authenticationService.login(any())).thenReturn(loginResult);
        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/login")
                        .with(csrf())
                        .content("{\"login\" : \"" + name + "\",\n" +
                                "\"password\" : \"" + password + "\"\n}")
                        .contentType("application/json"))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }
}