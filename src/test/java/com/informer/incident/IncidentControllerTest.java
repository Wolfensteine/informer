package com.informer.incident;

import com.informer.JsonConverter;
import com.informer.User;
import com.informer.authentication.AuthenticationToken;
import com.informer.token.Token;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(IncidentController.class)
class IncidentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IncidentService incidentService;

    @Test
    public void shouldRegisterNewIncident() throws Exception {
        //given
        String userToken = "userToken1";
        String informations = "informations123";
        User user = new User();
        user.setId(126);
        user.setLogin("userLoginAaa");
        Token token = new Token();
        token.setUserToken(userToken);
        token.setUser(user);
        NewIncidentInformations newIncidentInformations = new NewIncidentInformations(informations);
        when(incidentService.register(newIncidentInformations, new AuthenticationToken(userToken)))
                .thenReturn(new IncidentResult(IncidentStatus.NEW, informations));
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.put("/incident/register")
                        .with(csrf())
                        .header("userToken", userToken)
                        .content(JsonConverter.convertObjectToJson(newIncidentInformations))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\":\"NEW\"," +
                        "\"description\":" + informations + "}"))
                .andDo(print());
    }

    @Test
    public void shouldGetAllIncidentsForGivenUser() throws Exception {
        //given
        String userToken = "userToken1";
        String informations1 = "informations1";
        String informations2 = "informations2";
        when(incidentService.getIncidents(userToken)).thenReturn(Arrays.asList(
                new IncidentResult(IncidentStatus.NEW, informations1),
                new IncidentResult(IncidentStatus.NEW, informations2)));
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/incidents")
                        .header("userToken", userToken))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"status\":\"NEW\"," +
                        "\"description\":" + informations1 + "}," +
                        "{\"status\":\"NEW\"," +
                        "\"description\":" + informations2 + "}]"))
                .andDo(print()).andReturn();
    }
}