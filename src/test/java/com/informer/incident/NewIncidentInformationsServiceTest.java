package com.informer.incident;

import com.informer.User;
import com.informer.authentication.AuthenticationToken;
import com.informer.token.Token;
import com.informer.token.TokenDAO;
import com.informer.token.WrongTokenException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NewIncidentInformationsServiceTest {

    private static IncidentService service;
    private static IncidentRepository incidentRepository;
    private static TokenDAO tokenDAO;

    @BeforeAll
    public static void setUp() {
        incidentRepository = mock(IncidentRepository.class);
        tokenDAO = mock(TokenDAO.class);
        service = new IncidentService(incidentRepository, tokenDAO);
    }

    @Test
    public void shouldRegisterIncident() throws WrongTokenException {
        //given
        String incidentInformations = "blablabla";
        Token token = new Token();
        String userToken = "userToken";
        token.setUserToken(userToken);
        NewIncidentInformations newIncidentInformations = new NewIncidentInformations(incidentInformations);
        User user = new User();
        user.setId(7647);
        user.setLogin("name");
        token.setUser(user);
        when(tokenDAO.findTokenByUserToken(userToken)).thenReturn(Optional.of(token));
        when(incidentRepository.save(any())).thenReturn(new IncidentResult(IncidentStatus.NEW, incidentInformations));
        //when
        IncidentResult incident = service.register(newIncidentInformations, new AuthenticationToken(userToken));
        //then
        assertThat(incident.getStatus()).isEqualTo(IncidentStatus.NEW);
        assertThat(incident.getDescription()).isEqualTo(incidentInformations);
    }

    @Test
    public void shouldGetIncidentsForGivenUserToken() {
        //given
        String userToken = "exampleToken";
        int userId = 56;
        List<IncidentResult> expectedIncidents = new ArrayList<>();
        expectedIncidents.add(new IncidentResult(IncidentStatus.NEW, "dwadsa"));
        expectedIncidents.add(new IncidentResult(IncidentStatus.COMPLETED, "231edasfd32fd"));
        expectedIncidents.add(new IncidentResult(IncidentStatus.NEW, "dm3284ureshfbjshge"));
        when(incidentRepository.getIncidentsByUserId(userId)).thenReturn(expectedIncidents);
        List<IncidentResult> additionalIncidents = new ArrayList<>(List.of(
                new IncidentResult(IncidentStatus.COMPLETED, "dwads")));
        when(incidentRepository.getIncidentsByUserId(2)).thenReturn(additionalIncidents);
        Token token = new Token();
        token.setUserToken(userToken);
        User user = new User();
        user.setId(userId);
        token.setUser(user);
        when(tokenDAO.findTokenByUserToken(userToken)).thenReturn(Optional.of(token));
        //when
        List<IncidentResult> incidents = service.getIncidents(userToken);
        //then
        assertThat(incidents).containsOnly(expectedIncidents.toArray(new IncidentResult[0]));
    }

    @Test
    public void shouldNotRegisterIncidentWhenUserTokenNotFound() {
        //given
        String userToken = "exampleToken";
        int userId = 56;
        List<IncidentResult> otherUserIncidents = new ArrayList<>();
        otherUserIncidents.add(new IncidentResult(IncidentStatus.NEW, "dwadsa"));
        otherUserIncidents.add(new IncidentResult(IncidentStatus.COMPLETED, "231edasfd32fd"));
        otherUserIncidents.add(new IncidentResult(IncidentStatus.NEW, "dm3284ureshfbjshge"));
        when(incidentRepository.getIncidentsByUserId(userId)).thenReturn(otherUserIncidents);
        //when
        List<IncidentResult> foundIncidents = service.getIncidents(userToken);
        //then
        assertThat(foundIncidents).isEmpty();
    }
}
