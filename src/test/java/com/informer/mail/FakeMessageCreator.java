package com.informer.mail;

import org.springframework.mail.javamail.MimeMessageHelper;

import java.util.Properties;

public class FakeMessageCreator implements MessageCreator {
    @Override
    public String createAccountActivationMessage(MimeMessageHelper messageHelper, Properties properties) {
        return properties.getProperty("login", "") + " " +
                properties.getProperty("email", "") + " " +
                properties.getProperty("token", "");
    }
}
