package com.informer.mail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GmailUserConfirmationsSenderTest {

    private static GmailUserConfirmationsSender sender;
    private static FakeMailSender mailSender;

    @BeforeAll
    public static void setUp() {
        mailSender = new FakeMailSender();
        sender = new GmailUserConfirmationsSender(mailSender, new FakeMessageCreator());
        ReflectionTestUtils.setField(sender, "adressFrom", "testEmail");
    }

    @Test
    public void shouldSendActivationEmailAfterUserRegistration() throws IOException, MessagingException {
        //given
        String login = "login";
        String email = "email@domain.com";
        String token = UUID.randomUUID().toString();
        //when
        sender.sendAccountActivationMail(login, email, token);
        //then
        List<MimeMessage> sentMessages = mailSender.getSentMessages();
        assertThat(sentMessages.size()).isEqualTo(1);
        String messageContent = getTextFromMessage(sentMessages.get(0));
        assertThat(messageContent).contains(login);
        assertThat(messageContent).contains(email);
        assertThat(messageContent).contains(token);
    }

    private String getTextFromMessage(Message message) throws MessagingException, IOException {
        String result = "";
        if (message.isMimeType("text/plain")) {
            result = message.getContent().toString();
        } else if (message.isMimeType("multipart/*")) {
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            result = getTextFromMimeMultipart(mimeMultipart);
        }
        return result;
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart) throws MessagingException, IOException {
        StringBuilder result = new StringBuilder();
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result.append("\n").append(bodyPart.getContent());
                break;
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result.append(getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
            }
        }
        return result.toString();
    }
}
