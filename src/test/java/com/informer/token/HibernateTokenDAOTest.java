package com.informer.token;

import com.informer.User;
import com.informer.user.EmailAlreadyAssigned;
import com.informer.user.HibernateUserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class HibernateTokenDAOTest {
    @Autowired
    private TestEntityManager entityManager;

    private HibernateTokenDAO dao;
    private HibernateUserDAO userDao;
    private User user;

    @BeforeEach
    private void setUp() throws EmailAlreadyAssigned {
        dao = new HibernateTokenDAO(entityManager.getEntityManager());
        userDao = new HibernateUserDAO(entityManager.getEntityManager());
        User userToRegister = new User();
        int userId = 1;
        userToRegister.setId(userId);
        user = userDao.registerUser(userToRegister);
    }

    @Test
    void shouldFindUserTokenByUserId() {
        //given
        String userToken = "userToken";
        dao.save(new Token(userToken, user));
        //when
        Optional<Token> foundToken = dao.findTokenByUserId(user.getId());
        //then
        assertThat(foundToken).isNotEmpty();
        assertThat(foundToken)
                .hasValueSatisfying(token -> {
                    assertThat(token.getUserToken()).isEqualTo(userToken);
                    assertThat(token.getUser()).isEqualTo(user);
                });
    }

    @Test
    void findTokenByUserToken() throws EmailAlreadyAssigned {
        //given
        String userToken = "userToken";
        User userToRegister = new User();
        int userId = 1;
        userToRegister.setId(userId);
        User user = userDao.registerUser(userToRegister);
        dao.save(new Token(userToken, userToRegister));
        //when
        Optional<Token> foundToken = dao.findTokenByUserToken(userToken);
        //then
        assertThat(foundToken).isNotEmpty();
        assertThat(foundToken)
                .hasValueSatisfying(token -> {
                    assertThat(token.getUserToken()).isEqualTo(userToken);
                    assertThat(token.getUser()).isEqualTo(user);
                });
    }
}