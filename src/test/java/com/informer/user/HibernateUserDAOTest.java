package com.informer.user;

import com.informer.ClearDatabaseAction;
import com.informer.User;
import com.informer.authentication.LoginInformations;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import javax.persistence.NoResultException;
import javax.sql.DataSource;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class HibernateUserDAOTest {
    @Autowired
    private HibernateUserDAO dao;
    @Autowired
    private DataSource dataSource;

    @Bean


    @BeforeEach
    public void clearDatabase() throws SQLException {
        new ClearDatabaseAction(dataSource).run();
    }

    @Test
    public void shouldRegisterNotExistingUser() {
        //given
        User user = new User();
        user.setEmail("email");
        String login = "user1";
        String password = "password1";
        user.setLogin(login);
        user.setPassword(password);
        //when
        User registeredUser = dao.registerUser(user);
        //then
        assertThat(registeredUser.getLogin()).isEqualTo(login);
        assertThat(registeredUser.getPassword()).isEqualTo(password);
    }

    @Test
    public void shouldFindUser() {
        //given
        User user = new User();
        user.setEmail("email");
        String login = "user1";
        String password = "password1";
        user.setLogin(login);
        user.setPassword(password);
        dao.registerUser(user);
        //when
        User foundUser = dao.findUser(new LoginInformations(login, password));
        //then
        assertThat(foundUser.getLogin()).isEqualTo(login);
        assertThat(foundUser.getPassword()).isEqualTo(password);
    }

    @Test
    public void shouldGetExceptionWhenUserNotFound() {
        //given
        LoginInformations loginInformations = new LoginInformations("login2", "password2");
        //when
        //then
        assertThatThrownBy(() -> dao.findUser(loginInformations))
                .isInstanceOf(NoResultException.class);
    }
}