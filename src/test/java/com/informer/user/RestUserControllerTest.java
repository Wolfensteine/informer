package com.informer.user;

import com.informer.JsonConverter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RestUserController.class)
class RestUserControllerTest {
    @MockBean
    public UserService userService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldChangeEmail() throws Exception {
        //given
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("completelyNewEmail");
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/changeEmail")
                        .with(csrf())
                        .header("userToken", "userToken")
                        .content(JsonConverter.convertObjectToJson(changeEmailDetails))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void shouldNotChangeEmailWhenUserNotFound() throws Throwable {
        //given
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("completelyNewEmail");
        String userToken = "userTokenNotInDatabase";
        doThrow(new UserNotFound(userToken)).when(userService).changeEmail(userToken, changeEmailDetails);
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/changeEmail")
                        .with(csrf())
                        .header("userToken", userToken)
                        .content(JsonConverter.convertObjectToJson(changeEmailDetails))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void shouldNotChangeEmailWhenSameEmailInDatabase() throws Exception, WrongEmailException {
        //given
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("completelyNewEmail");
        String userToken = "userTokenNotInDatabase";
        doThrow(new EmailAlreadyAssigned(changeEmailDetails.getNewEmail()))
                .when(userService).changeEmail(userToken, changeEmailDetails);
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/changeEmail")
                        .with(csrf())
                        .header("userToken", userToken)
                        .content(JsonConverter.convertObjectToJson(changeEmailDetails))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void shouldChangePassword() throws Exception {
        //given
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("currentPassword",
                "newPassword");
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/changePassword")
                        .with(csrf())
                        .header("userToken", "userToken")
                        .content(JsonConverter.convertObjectToJson(changePasswordDetails))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void shouldNotChangePasswordWhenUserNotFound() throws Throwable {
        //given
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("currentPassword",
                "newPassword");
        String userToken = "userTokenNotInDatabase";
        doThrow(new UserNotFound(userToken)).when(userService).changePassword(userToken, changePasswordDetails);
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/changePassword")
                        .with(csrf())
                        .header("userToken", userToken)
                        .content(JsonConverter.convertObjectToJson(changePasswordDetails))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void shouldNotChangePasswordIfWrongCurrentPasswordProvided() throws Throwable {
        //given
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("currentPassword",
                "newPassword");
        String userToken = "userToken";
        doThrow(new WrongPasswordException()).when(userService).changePassword(userToken, changePasswordDetails);
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/changePassword")
                        .with(csrf())
                        .header("userToken", userToken)
                        .content(JsonConverter.convertObjectToJson(changePasswordDetails))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void shouldRegisterNewUser() throws Exception {
        //given
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails("login", "email", "password");
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                        .with(csrf())
                        .content(JsonConverter.convertObjectToJson(userRegistrationDetails))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andDo(print());
        verify(userService).registerUser(userRegistrationDetails);
    }

    @Test
    public void shouldNotRegisterUserIfAnotherUserWithSameLoginExists() throws Exception {
        //given
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails("login", "email", "password");
        doThrow(new EmailAlreadyAssigned("email")).when(userService).registerUser(userRegistrationDetails);
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                        .with(csrf())
                        .content(JsonConverter.convertObjectToJson(userRegistrationDetails))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Email exists"))
                .andDo(print());
    }

    @Test
    public void shouldNotRegisterUserIfAnotherUserWithSameEmailExists() throws Exception {
        //given
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails("login", "email", "password");
        doThrow(new LoginAlreadyAssigned("login")).when(userService).registerUser(userRegistrationDetails);
        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/user/register")
                        .with(csrf())
                        .content(JsonConverter.convertObjectToJson(userRegistrationDetails))
                        .contentType("application/json"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Login exists"))
                .andDo(print());
    }
}