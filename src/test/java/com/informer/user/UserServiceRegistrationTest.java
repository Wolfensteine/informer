package com.informer.user;

import com.informer.User;
import com.informer.mail.UserConfirmationsSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.mail.MessagingException;
import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

public class UserServiceRegistrationTest {
    private UserDAO userDAO;
    private UserService service;
    private UserActivationsDAO activationDAO;

    @BeforeEach
    public void setUp() {
        userDAO = mock(UserDAO.class);
        activationDAO = mock(UserActivationsDAO.class);
        service = new UserService(userDAO, new UserActivationService(mock(UserConfirmationsSender.class),
                activationDAO));
    }

    @Test
    public void shouldRegisterUser() throws LoginAlreadyAssigned, EmailAlreadyAssigned, MessagingException {
        //given
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails("login", "email",
                "password");
        User user = new User();
        user.setLogin(userRegistrationDetails.getLogin());
        user.setEmail(userRegistrationDetails.getEmail());
        user.setPassword(userRegistrationDetails.getPassword());
        when(activationDAO.addUserActivation(user)).thenReturn(new UserActivation());
        //when
        service.registerUser(userRegistrationDetails);
        //then
        verify(userDAO).registerUser(user);
    }

    @Test
    public void shouldNotRegisterUserIfAnotherUserWithSameLoginExists() {
        //given
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails("login", "email",
                "password");
        User user = new User();
        user.setLogin(userRegistrationDetails.getLogin());
        user.setEmail(userRegistrationDetails.getEmail());
        user.setPassword(userRegistrationDetails.getPassword());


        when(userDAO.findUsersByLoginOrEmail(userRegistrationDetails.getLogin(), userRegistrationDetails.getEmail()))
                .thenReturn(Arrays.asList(createUser("login1", "email2"),
                        createUser("login", "email22"), createUser("login", "emaildas")));
        //when then
        assertThatThrownBy(() ->
                service.registerUser(userRegistrationDetails))
                .isInstanceOf(LoginAlreadyAssigned.class)
                .hasMessageContaining(userRegistrationDetails.getLogin());
    }

    @Test
    public void shouldNotRegisterUserIfAnotherUserWithSameEmailExists() {
        //given
        UserRegistrationDetails userRegistrationDetails = new UserRegistrationDetails("login", "email",
                "password");
        User user = new User();
        user.setLogin(userRegistrationDetails.getLogin());
        user.setEmail(userRegistrationDetails.getEmail());
        user.setPassword(userRegistrationDetails.getPassword());
        //when
        when(userDAO.findUsersByLoginOrEmail(userRegistrationDetails.getLogin(), userRegistrationDetails.getEmail()))
                .thenReturn(Arrays.asList(createUser("login1", "email"),
                        createUser("losgin", "email22"), createUser("l2ogin", "email")));
        //then
        assertThatThrownBy(() ->
                service.registerUser(userRegistrationDetails))
                .isInstanceOf(EmailAlreadyAssigned.class)
                .hasMessageContaining(userRegistrationDetails.getEmail());
    }

    private User createUser(String login, String email) {
        User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        return user;
    }
}
