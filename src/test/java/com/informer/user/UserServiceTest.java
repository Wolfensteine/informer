package com.informer.user;


import com.informer.User;
import com.informer.mail.UserConfirmationsSender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

class UserServiceTest {
    private UserDAO userDAO;
    private UserService service;

    @BeforeEach
    public void setUp() {
        userDAO = mock(UserDAO.class);
        service = new UserService(userDAO, new UserActivationService(mock(UserConfirmationsSender.class),
                mock(UserActivationsDAO.class)));
    }

    @Test
    public void shouldPerformChangeEmailAction() throws UserNotFound, WrongEmailException, EmailAlreadyAssigned {
        //given
        String userToken = "userToken";
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("completelyNewEmail");
        User user = new User();
        user.setEmail("");
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.of(user));
        //when
        service.changeEmail(userToken, changeEmailDetails);
        //then
        verify(userDAO).changeEmail(user);
    }

    @Test
    public void shouldNotPerformChangeEmailActionWhenUserNotFound() {
        String userToken = "userTokenNotInDatabase";
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("completelyNewEmail");
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.empty());
        //when then
        assertThatThrownBy(() ->
                service.changeEmail(userToken, changeEmailDetails))
                .isInstanceOf(UserNotFound.class);
        verify(userDAO, times(0)).changeEmail(any(User.class));
    }

    @Test
    public void shouldNotPerformChangeEmailActionWhenSameEmailProvided() {
        String userToken = "userToken";
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("actualEmail");
        User user = new User();
        user.setEmail("actualEmail");
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.of(user));
        //when then
        assertThatThrownBy(() ->
                service.changeEmail(userToken, changeEmailDetails))
                .isInstanceOf(WrongEmailException.class);
        verify(userDAO, times(0)).changeEmail(any(User.class));
    }

    @Test
    public void shouldNotPerformPasswordActionIfEmailAssignedToOtherUser() {
        //given
        String userToken = "userToken";
        ChangeEmailDetails changeEmailDetails = new ChangeEmailDetails("otherEmailAlreadyInDatabase");
        User user = new User();
        user.setEmail("actualEmail");
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.of(user));
        when(userDAO.emailRegistered("otherEmailAlreadyInDatabase")).thenReturn(true);
        //when then
        assertThatThrownBy(() ->
                service.changeEmail(userToken, changeEmailDetails))
                .isInstanceOf(EmailAlreadyAssigned.class);
    }

    @Test
    public void shouldPerformChangePasswordAction() throws UserNotFound, WrongPasswordException {
        //given
        String userToken = "userToken";
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("currentPassword",
                "newPassword");
        User user = new User();
        user.setEmail("actualEmail");
        user.setPassword(changePasswordDetails.getCurrentPassword());
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.of(user));
        //when
        service.changePassword(userToken, changePasswordDetails);
        //then
        verify(userDAO).changePassword(user);
    }

    @Test
    public void shouldNotPerformChangePasswordActionIfUserNotFound() {
        //given
        String userToken = "userToken";
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("currentPassword",
                "newPassword");
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.empty());
        //when then
        assertThatThrownBy(() ->
                service.changePassword(userToken, changePasswordDetails))
                .isInstanceOf(UserNotFound.class);
    }

    @Test
    public void shouldNotPerformChangePasswordActionIfWrongCurrentPasswordProvided() {
        //given
        String userToken = "userToken";
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("notCurrentPassword",
                "newPassword");
        User user = new User();
        user.setPassword("currentPassword");
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.of(user));
        //when then
        assertThatThrownBy(() ->
                service.changePassword(userToken, changePasswordDetails))
                .isInstanceOf(WrongPasswordException.class);
    }

    @Test
    public void shouldNotPerformChangePasswordActionIfCurrentPasswordIsEqualToTheNew() {
        //given
        String userToken = "userToken";
        ChangePasswordDetails changePasswordDetails = new ChangePasswordDetails("currentPassword",
                "currentPassword");
        User user = new User();
        user.setPassword(changePasswordDetails.getCurrentPassword());
        when(userDAO.findUserByUserToken(userToken)).thenReturn(Optional.of(user));
        //when then
        assertThatThrownBy(() ->
                service.changePassword(userToken, changePasswordDetails))
                .isInstanceOf(WrongPasswordException.class);
    }
}